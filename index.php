<?php session_start();
require_once 'Helper.php';
require 'validation.php';

if(Helper::_get("logout") == true) {
    setcookie("l", "", time()-3600);
	unset($_SESSION['isLogin']);
	session_destroy();
}

header("Location:main.php");
