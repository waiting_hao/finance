<?php
require_once 'validation.php';
require_once 'Db.php';
require_once 'addExpense.php';
require_once 'Helper.php';
define("PAGE_TITLE", "记账");

$d = Helper::_get("d");
$db = new Db();

if(!empty($d) && is_numeric($d))
{
    $detail = $db->getDetailById($d);
    $detail['datetime'] = date("Y-m-d\TH:i:s", strtotime($detail['datetime']));
}
if(!isset($detail))
{
    $detail = array();
    $detail['datetime'] = date("Y-m-d\TH:i:s");
    $detail['comment'] = null;
    $detail['money'] = null;
    $detail['member'] = null;
    $detail['id'] = null;
}
 ?>
<!DOCTYPE html>
<html>
	<head>
		<?php require 'head.php'; ?>
		<script src="js/main.js"></script>
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<?php require 'header.php';?>
			</div>
			<div data-role="content">

				<form method="post" action="">
                    <input type="hidden" name="id" value="<?php echo $detail['id']; ?>"/>
					<label for="money" class="ui-hidden-accessible">金额</label>
					<input type="number" name="money" step="0.01" pattern="[0-9]+(\.[0-9]+)?" id="money" placeholder="金额" value="<?php echo $detail['money']; ?>" required="required"/>
					<label for="comment" class="ui-hidden-accessible">备注</label>
					<input type="text" name="comment" id="comment" placeholder="备注" value="<?php echo $detail['comment']; ?>" required="required"/>
					<label for="datetime" class="ui-hidden-accessible">日期时间</label>
					<input type="datetime-local" name="datetime" id="datetime" step="1" value="<?php echo $detail['datetime']; ?>" required="required"/>
                    <?php
                    $result = $db->getAllUsers();
                    ?>
					<fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
						<legend>消费成员：</legend>
                        <?php
                        $members = array();
                        if(!empty($detail['member']))
                        {
                            $members = explode(",", $detail['member']);
                        }
                        foreach($result as $member)
                        {
                            $checked = "";
                            if(in_array($member['id'], $members))
                            {
                                $checked = "checked='checked'";
                            }
                            echo "<label for='checkbox_{$member['username']}'>{$member['name']}</label>";
                            echo "<input type='checkbox' $checked name='member[]' id='checkbox_{$member['username']}' value='{$member['id']}'>";
                        }
                        ?>
					</fieldset>
                    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
                        <legend>付款人：</legend>
                        <?php
                        foreach($result as $member)
                        {
                            $checked = "";
                            if(isset($detail['payer']) && $member['id'] == $detail['payer'])
                            {
                                $checked = "checked='checked'";
                            }
                            echo "<input type='radio' name='payer' id='radio_{$member['username']}' value='{$member['id']}' $checked>";
                            echo "<label for='radio_{$member['username']}'>{$member['name']}</label>";
                        }
                        ?>
                    </fieldset>
					<input type="submit" data-inline="true" value="提交">
				</form>
			</div>
			<div data-role="footer">
				<?php require 'footer.php'; ?>
			</div>
		</div>
	</body>
</html>