(function($, window, undefined) {
	$.widget("mobile.listview", $.mobile.listview, {
		options: {
			childPages: true
		}
	});
})(jQuery, this);