<?php define("PAGE_TITLE", "登录") ?>
<!DOCTYPE html>
<html>
	<head>
		<?php require 'head.php'; ?>
	</head>
	<body>
		<div data-role="page">
			<form action="index.php" method="post">
				<label for="username" class="ui-hidden-accessible">用户名：</label>
				<input type="text" name="username" id="username" placeholder="用户名" value=""/>
				<input type="password" name="password" id="password" placeholder="密码" value=""/>
				<input type="submit" value="登录" data-theme="a"/>
			</form>
			<?php require 'footer.php'; ?>
		</div>
	</body>
</html>