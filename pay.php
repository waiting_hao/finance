<?php
/**
 * Created by PhpStorm.
 * User: 冠豪
 * Date: 2015/4/29
 * Time: 21:19
 */
require_once 'globe.php';
require_once 'validation.php';
require_once 'Db.php';
require_once 'addExpense.php';
define("PAGE_TITLE", "账单");

$y = Helper::_get('y');
if(empty($y) && !is_numeric($y))
{
    $y = date("Y");
}
$m = Helper::_get('m');
if(empty($m) && !is_numeric($m))
{
    $m = date("m");
}
while($m > 12)
{
    $y++;
    $m -= 12;
}
while($m <= 0)
{
    $y--;
    $m += 12;
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php require 'head.php'; ?>
    <script src="js/main.js"></script>
</head>
<body>
<div data-role="page">
    <div data-role="header">
        <?php require 'header.php';?>
    </div>
    <div class="ui-grid-b" data-role="header">
        <a data-transition="slide" data-direction="reverse" href="?y=<?php echo $y ?>&m=<?php echo $m - 1 ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-carat-l ui-btn-icon-notext ui-btn-inline">Button</a>
        <h1><?php echo "$y-$m";?></h1>
        <a data-transition="slide" href="?y=<?php echo $y ?>&m=<?php echo $m + 1 ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-carat-r ui-btn-icon-notext ui-btn-inline">Button</a>
        <div data-role="navbar">
            <ul>
                <li><a data-transition="flip" data-direction="reverse" href="bill.php?y=<?php echo $y ?>&m=<?php echo $m ?>">账单</a></li>
                <li><a class="ui-btn-active ui-state-persist">月结</a></li>
            </ul>
        </div>
    </div>
    <div data-role="content">
        <?php
        $db = new Db();
        $result = $db->getStatement($y, $m);
        //print_r($result);
        ?>
        <ul data-role="listview" data-inset="true">
            <?php
            foreach ($result as $member) {
                $isPay = "应付";
                if($member['money'] < 0)
                {
                    $member['money'] = number_format(abs($member['money']), 2);
                    $isPay = "应收";
                } else {
                    $member['money'] = number_format($member['money'], 2);
                }
            echo "
                <li>
                    <a data-transition='flow' href='bill.php?y=$y&m=$m&u={$member['userId']}'>

                    <table class='ui-responsive'>
                        <tr><th>{$member['name']}</th><th></th><th></th><th></th></tr>
                        <tr><th></th><th>消费</th><th>付款</th><th>{$isPay}</th></tr>
                        <tr><td></td><td>{$member['consume']}</td><td>{$member['pay']}</td><td>{$member['money']}</td></tr>
                    </table>

                    </a>
                </li>";
            }
            ?>
        </ul>
    </div>
    <div data-role="footer">
        <?php require 'footer.php'; ?>
    </div>
</div>
</body>
</html>