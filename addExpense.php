<?php
/**
 * Created by PhpStorm.
 * User: 冠豪
 * Date: 2015/4/22
 * Time: 22:08
 */
require_once 'globe.php';
require_once 'validation.php';
require_once 'Db.php';
require_once 'Helper.php';

if(isset($_POST["money"]) && isset($_POST["comment"]) && isset($_POST["datetime"])
    && isset($_POST['member']) && isset($_POST['payer']))
{
    $form = array();
    $form['money'] = $_POST["money"];
    $form['comment'] = $_POST["comment"];
    $form['datetime'] = $_POST["datetime"];
    $form['member'] = $_POST['member'];
    $form['payer'] = $_POST['payer'];
    $db = new Db();

    $form['id'] = Helper::_post("id");
    if(empty($form['id']) || !is_numeric($form['id']))
    {
        $result = $db->addExpense($form);
    }
    else
    {
        $result = $db->updateDetail($form);
    }
    if($result)
    {
        header("Location:bill.php");
    }
}
