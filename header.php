<?php
$pageId = uniqid();

if(!defined('CURRENT_PAGE') || CURRENT_PAGE != "main")
{
	echo '<a href="index.php" data-role="button" data-icon="home">首页</a>';
}

?>
<h1><?php echo defined('PAGE_TITLE') ? PAGE_TITLE : ''?></h1>
<?php
if(defined('CURRENT_PAGE') && CURRENT_PAGE == "main")
{
	echo '<a href="index.php?logout=true" data-role="button" data-icon="home">退出</a>';
}
?>
<a href="#popupMenu<?php echo $pageId ?>" data-rel="popup" data-transition="slidedown" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-icon-gear ui-btn-icon-left ui-btn-a">菜单</a>
<div data-role="popup" id="popupMenu<?php echo $pageId ?>" data-theme="a">
    <ul data-role="listview" data-inset="true" style="min-width:100px;">
        <li><a href="accounting.php">记账</a></li>
        <li><a href="bill.php">账单</a></li>
        <li><a href="pay.php">月结</a></li>
    </ul>
</div>