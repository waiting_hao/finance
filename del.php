<?php
require_once 'globe.php';
require_once 'validation.php';
require_once 'Db.php';
require_once 'Helper.php';

header('Content-type: text/json');
$result = array();

$id = Helper::_post("id");
if(empty($id) || !is_numeric($id))
{
    $result['success'] = false;
    $result['msg'] = "id参数不正确";
    echo json_encode($result);
    return;
}
$db = new Db();
$delResult = $db->delDetail($id);
if($delResult)
{
    $result['success'] = true;
}
else
{
    $result['success'] = false;
}
echo json_encode($result);