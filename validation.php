<?php
require_once 'Helper.php';
if(!isset($_SESSION))
{
    session_start();
}
require_once 'Db.php';
function valide()
{
	if(isset($_SESSION['isLogin']) && $_SESSION['isLogin'] == true)
	{
		return true;
	}

    $db = new Db();
    $user = $db->validateAccount(Helper::_post('username'), Helper::_post("password"));
    if(empty($user))
    {
        $user = $db->validateAccountBySession($_COOKIE["l"]);
    }
	if($user)
	{
		$_SESSION['isLogin'] = true;
        $_SESSION['userId'] = $user['id'];
        $session = uniqid();
        $db->updateAutologin($user['id'], $session);
        setcookie("l", $session, time()+(3600*24*30));
		return true;
	}


	return false;
}
if(!valide())
{
    header("Location:login.php");
}