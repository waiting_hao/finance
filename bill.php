<?php
require_once 'globe.php';
require_once 'validation.php';
require_once 'Db.php';
require_once 'addExpense.php';
require_once 'Helper.php';

$pageId = uniqid();
$userId = Helper::_get("u");
$db = new Db();
if(!empty($userId) && is_numeric($userId))
{
    $name = $db->getNameById($userId);
}
$title = "账单";
if(!empty($name))
{
    $title .= "（{$name}）";
}
define("PAGE_TITLE", $title);

$y = Helper::_get('y');
if(empty($y) && !is_numeric($y))
{
    $y = date("Y");
}
$m = Helper::_get('m');
if(empty($m) && !is_numeric($m))
{
    $m = date("m");
}
while($m > 12)
{
    $y++;
    $m -= 12;
}
while($m <= 0)
{
    $y--;
    $m += 12;
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php require 'head.php'; ?>
    <script src="js/main.js"></script>
</head>
<body>
<div data-role="page">
    <div data-role="header">
        <?php require 'header.php';?>
    </div>
    <div class="ui-grid-b" data-role="header">
        <a data-transition="slide" data-direction="reverse" href="?y=<?php echo $y ?>&m=<?php echo $m - 1 ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-carat-l ui-btn-icon-notext ui-btn-inline">Button</a>
        <h1><?php echo "$y-$m";?></h1>
        <a data-transition="slide" href="?y=<?php echo $y ?>&m=<?php echo $m + 1 ?>" class="ui-shadow ui-btn ui-corner-all ui-icon-carat-r ui-btn-icon-notext ui-btn-inline">Button</a>
        <div data-role="navbar">
            <ul>
                <li><a class="ui-btn-active ui-state-persist">账单</a></li>
                <li><a data-transition="flip" href="pay.php?y=<?php echo $y ?>&m=<?php echo $m ?>">月结</a></li>
            </ul>
        </div>
    </div>
    <div data-role="content">
        <script>
            var detailId;
            function clickDelBtn(money, payer, id) {
                $(".money_del_dialog").text(money);
                $(".payer_del_dialog").text(payer);
                detailId = id;
            }

            function delDetail() {
                $.ajax({
                    url: "del.php",
                    type: "POST",
                    async: false,
                    data: {
                        id: detailId
                    },
                    success: function (data) {
                        if(data.success) {
                            $(".record_wrep_" + detailId).remove();
                        }
                    }
                });
            }
        </script>
        <div data-role="collapsible-set" data-inset="false">
            <?php
            if(empty($userId) || !is_numeric($userId))
            {
                $result = $db->getBill($y, $m);
            }
            else
            {
                $result = $db->getPersonBill($y, $m, $userId);
            }
            foreach($result as $detail)
            {
                $date = $detail['datetime'];
                $date = date("Y/m/d", strtotime($date));

                echo "<div data-role='collapsible' class='record_wrep_{$detail['id']}'>";
                echo "<h3>金额：{$detail['money']} 日期：{$date}</h3>";
                echo "<p>
                        成员：{$detail['members']}<br/>
                        备注：{$detail['comment']}<br/>
                        付款人：{$detail['payer']}<br/>
                        <a href='accounting.php?d={$detail['id']}' data-role='button' data-inline='true'>编辑</a>
                        <a href='#popupDialog_{$pageId}' onclick='clickDelBtn(\"{$detail['money']}\", \"{$detail['payer']}\", \"{$detail['id']}\");' data-rel='popup' data-position-to='window' data-transition='pop'  data-role='button' data-inline='true'>删除</a>
                        </p>";
                echo '</div>';
            }
            ?>
        </div>
        <div data-role="popup" class="fin_dialog" id="popupDialog_<?php echo $pageId ?>" data-overlay-theme="a" data-theme="a" data-dismissible="false" style="max-width:400px;">
            <div data-role="header" data-theme="a">
                <h1>删除</h1>
            </div>
            <div role="main" class="ui-content">
            <h3 class="ui-title">确认要删除此记录?</h3>
                <p>金额：<span class="money_del_dialog"></span></p>
                <p>付款人：<span class="payer_del_dialog"></span></p>
                <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-a" data-rel="back">取消</a>
                <a href="#" onclick="delDetail();" class="ui-btn ui-corner-all ui-shadow ui-btn-inline ui-btn-a" data-rel="back" data-transition="flow">删除</a>
            </div>
        </div>
    </div>
    <div data-role="footer">
        <?php require 'footer.php'; ?>
    </div>
</div>

</body>
</html>