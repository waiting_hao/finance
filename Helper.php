<?php
/**
 * Created by PhpStorm.
 * User: 冠豪
 * Date: 2015/4/29
 * Time: 22:12
 */

class Helper {
    static public function _post($index)
    {
        return isset($_POST[$index]) ? $_POST[$index] : null;
    }

    static public function _get($index)
    {
        return isset($_GET[$index]) ? $_GET[$index] : null;
    }
}