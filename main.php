<?php 
require 'validation.php';

define("PAGE_TITLE", "主页-合租记账");
define("CURRENT_PAGE", "main");
 ?>
<!DOCTYPE html>
<html>
	<head>
		<?php require 'head.php'; ?>
		<script src="js/main.js"></script>
	</head>
	<body>
		<div data-role="page">
			<div data-role="header">
				<?php require 'header.php';?>
			</div>
			<div data-role="content">
				<ul data-role="listview" data-inset="true">
					<li <data-role="list-divider" data-theme="b">
						<h1>请选择</h1>
					</li>
					<li>
						<a href="accounting.php" data-transition="pop">记账</a>
					</li>
					<li>
                        <a href="bill.php">账单</a>
					</li>
				</ul>
			</div>
			<div data-role="footer">
				<?php require 'footer.php'; ?>
			</div>
		</div>
	</body>
</html>